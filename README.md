#### This project is for "Containers - Docker" 

repository: https://gitlab.com/twn-devops-bootcamp/latest/07-docker/docker-exercises

EXERCISE 0: Clone Git repository and create your own1

EXERCISE 1: Start Mysql container

 Test the application locally with a mysql database
 Start it as a docker container:

1. Start mysql container locally using the official Docker image. Set all needed environment variables.
2. Export all needed environment variables for your application for connecting with the    database (check variable names inside the code).
3. Build a jar file and start the application. Test access from browser. Make some changes.

EXERCISE 2: Start Mysql GUI container
Deploy phpmyadmin, to access the database data using a UI tool.

1. Start phpmyadmin container using the official image.
2. Access phpmyadmin from your browser and test logging in to your Mysql database

EXERCISE 3: Use docker-compose for Mysql and Phpmyadmin
The apps require 2 containers. Configure a docker-compose file for both:

1. Create a docker-compose file with both containers
2. Configure a volume for your DB
3. Test that both containers are working


EXERCISE 4: Dockerize your Java Application
Create a Dockerfile for your java application

EXERCISE 5: Build and push Java Application Docker Image
1. Create a docker hosted repository on Nexus
2. Build the image locally and push to this repository

EXERCISE 6: Add application to docker-compose
Add your application's docker image to docker-compose. Configure all needed env vars.

EXERCISE 7: Run application on server with docker-compose
1. Set insecure docker repository on server, because Nexus uses http
2. Run docker login on the server to be allowed to pull the image
3. Your application index.html has a hardcoded localhost as a HOST to send requests to the backend. You need to fix that and set the server IP address instead, because the server is going to be the host when you deploy the application on a remote server. (Don't forget to rebuild and push the image and if needed adjust the docker-compose file)
4. Copy docker-compose.yaml to the server
5. Set the needed environment variables for all containers in docker-compose
6. Run docker-compose to start all 3 containers

